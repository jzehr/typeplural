PennController.Template( 
    PennController.GetTable("table.csv").filter(row=>Number(row.Item)<41)
    ,
    row =>
    PennController( "practice-"+words[row.Item].class ,
        
        defaultTooltip
            .settings.label("")                 // No "OK" label at the bottom-right
            .settings.position("middle right")  // To the right of elements by default
            .settings.key(13,"no click")        // 13 is keycode for enter/return
        ,
        defaultTextInput
            .settings.length(5)                 // 5 letter words
            .settings.size("5em")               // 5em wide
            .settings.center()                  // Center TextInputs on the screen
        ,
        newTooltip("nope", "This doesn't seem quite right")     // Negative feedback (not printed yet)
            .settings.css('color', 'darksalmon')
        ,
        newCanvas("singular", 200, 300)         // Singular canvas (image + text)
            .settings.add(0,0,newImage("sgfigure", row.SgImg).settings.size(200,200))
            .settings.add("center at 50%",200,newText("sgtext","There is one "+words[row.Item].sg))
        ,
        newCanvas("plural", 200, 300)         // Plural canvas (image + text)
            .settings.add(0,0,newImage("plfigure", row.PlImg).settings.size(200,200))
            .settings.add("center at 50%",200,newText("pltext","There are two "+words[row.Item].pl))
        ,
        newCanvas("main", "90vw", "300px")   // Canvas container singular and plural, side by side
            .settings.add( "center at 33%" , "middle at 50%" , getCanvas("singular") )
            .settings.add( "center at 66%" , "middle at 50%" , getCanvas("plural") )
            .print()
        ,
        newTooltip("Press Enter to continue")
            .settings.position("bottom center")
            .print( getCanvas("main") )
            .wait()
        ,
        newTimer(100).start().wait()    // 100ms delay prevents just-pressed Enter from adding a line to new TextInput
        ,
        newTextInput("sginput", "")
            .settings.before( newText("There is one&nbsp;") )
            .print()
        ,
        newTooltip("Press Enter to validate").print( getTextInput("sginput") )  // Instruction next to tooltip
        ,
        getTextInput("sginput")
            .wait( getTextInput("sginput").test.text(words[row.Item].sg)                // Validate only if answer is right
                        .failure( getTooltip("nope").print(getTextInput("sginput")) ) ) // negative feedback if wrong
            .settings.disable()         // Disable now that answer is right
        ,
        newTimer(100).start().wait()    // 100ms delay prevents just-pressed Enter from adding a line to new TextInput
        ,
        newTextInput("plinput", "")
            .settings.before( newText("There are two&nbsp;") )
            .print()
        ,
        newTooltip("Press Enter to validate").print( getTextInput("plinput") )  // Instruction next to tooltip
        ,
        getTextInput("plinput")
            .wait( getTextInput("plinput").test.text(words[row.Item].pl)                // Validate only if answer is right
                        .failure( getTooltip("nope").print(getTextInput("plinput")) ) ) // negative feedback if wrong
            .settings.disable()         // Disable now that answer is right
        ,
        newTimer(500).start().wait()    // Wait 500ms before going to next trial
            
    )
    .log("id", PennController.GetURLParameter("id"))
    .log("Item", row.Item)
    .log("Class", words[row.Item].class)
    .log("Sg", words[row.Item].sg)
    .log("Pl", words[row.Item].pl)
)