PennController.ResetPrefix(null)

PennController.SendResults("send")

PennController.Sequence(
    "consent","instructions"
    ,
    randomize(startsWith("practice")),
    "half",
    randomize(startsWith("test"))
    ,
    "feedback","send","completion"
)

PennController.AddHost("https://lh3.googleusercontent.com/u/0/d/")