PennController.Template(
    "table.csv"
    ,
    row=>
    PennController( "test-"+words[row.Item].class ,
        
        defaultTooltip
            .settings.label("")                 // No "OK" label at the bottom-right
            .settings.position("middle right")  // To the right of elements by default
            .settings.key(13,"no click")        // 13 is keycode for enter/return
        ,
        defaultTextInput
            .settings.length(5)                 // 5 letter words
            .settings.size("5em")               // 5em wide
            .settings.center()                  // Center TextInputs on the screen
        ,
        newCanvas("singular", 200, 300)         // Singular canvas (image only)
            .settings.add(0,0,newImage("sgfigure",row.SgImg).settings.size(200,200))
        ,
        newCanvas("plural", 200, 300)         // Plural canvas (image only)
            .settings.add(0,0,newImage("plfigure",row.PlImg).settings.size(200,200))
        ,
        newCanvas("main", "90vw", "300px")   // Canvas container singular and plural images, side by side
            .settings.add( "center at 33%" , "middle at 50%" , getCanvas("singular") )
            .settings.add( "center at 66%" , "middle at 50%" , getCanvas("plural") )
            .print()
        ,
        newText("sgtext", "There is one "+words[row.Item].sg) // Print text below images already  
            .settings.center()
            .print()
        ,
        newTooltip("Press Enter").print( getText("sgtext") ).wait()  // Instruction next to text
        ,
        newTimer(100).start().wait()    // 100ms delay prevents just-pressed Enter from adding a line to new TextInput
        ,
        newTextInput("plinput", "")
            .settings.before( newText("There are two&nbsp;") )
            .settings.log("final")
            .print()
        ,
        newTooltip("Press Enter to validate").print( getTextInput("plinput") )  // Instruction next to tooltip
        ,
        newTooltip("nope", "Oops, this doesn't seem to match with the singular").settings.css('color', 'darksalmon'),
        newTooltip("badlength", "This must be a 5-letter word").settings.css('color', 'darksalmon')
        ,
        getTextInput("plinput")
            .wait(                                                                               // Check before validating that:
                getTextInput("plinput").testNot.text("")                                         //   i. text is not empty
                .and( getTextInput("plinput").test.text(RegExp("^"+roots[row.Item-1]))   //  ii. first 3 characters match singular
                    .failure( 
                        getText("sgtext").settings.text("There is one "+words[row.Item].sg.replace(/^(...)(..)$/,"<strong>$1</strong>$2"))  // Bolden root
                        ,
                        getTooltip("nope").settings.css("transform","translateX(25px)").print(getTextInput("plinput"))                      // 25px further to the right
                    )
                )
                .and( getTextInput("plinput").test.text(/^\w{5}$/)                               // iii. 5-letter response
                    .failure( 
                        getTooltip("badlength").settings.css("transform","translateX(25px)").print(getTextInput("plinput"))                 // 25px further to the right
                        ,
                        getTooltip("nope").test.printed()
                            .success( 
                                getTooltip("badlength").settings.css("transform","translateX(25px) translateY(-15px)"),                     // Adjust Y offset if both tooltips
                                getTooltip("nope").settings.css("transform","translateX(25px) translateY(15px)")                            // are printed at the same time
                            )
                    )
                )
            )
            .settings.disable()         // Disable now that answer is right
        ,
        newTimer(500).start().wait()    // Wait 500ms before going to next trial
            
    )
    .log("id", PennController.GetURLParameter("id"))
    .log("Item", row.Item)
    .log("Class", words[row.Item].class)
    .log("Sg", words[row.Item].sg)
    .log("Pl", words[row.Item].pl)

)