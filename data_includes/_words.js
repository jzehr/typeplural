// We will store the roots, affixes, singular and plural forms in js arrays
var roots = [], affixes = [], words = [];

// We use Template to read the roots and affixes from the table
PennController.Template(row=>{
    roots.push(row.Roots);
    if (row.Affixes) affixes.push(row.Affixes);
    return [];
})

// We use Template again now that roots has been filled
PennController.Template(row=>{
    if (words.length) return [];
    fisherYates[roots];
    fisherYates[affixes];
    for (let i = 0; i < 40; i++){
        if (i<20){
            words[i+1] = {sg: roots[i]+affixes[0]};
            words[i+41] = {sg: roots[i+40]+affixes[0]};
            if (i<6){
                // A>B: practice items 1-6 + test items 41-46
                words[i+1].pl = roots[i]+affixes[1];
                words[i+41].pl = roots[i+40]+affixes[1];
                words[i+1].class = "ab";
                words[i+41].class = "ab";
            }
            else{
                // A>C: practice items 7-20 + test items 47-60
                words[i+1].pl = roots[i]+affixes[2];
                words[i+41].pl = roots[i+40]+affixes[2];
                words[i+1].class = "ac";
                words[i+41].class = "ac";
            }
        }
        else{
            // D>E: practice items 21-40 + test items 61-80
            words[i+1] = {sg: roots[i]+affixes[3], pl: roots[i]+affixes[4]};
            words[i+41] = {sg: roots[i+40]+affixes[3], pl: roots[i+40]+affixes[4]};
            words[i+1].class = "de";
            words[i+41].class = "de";
        }
    }
    return [];
})