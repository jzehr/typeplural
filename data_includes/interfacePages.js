PennController("completion",
    newText("<p>Please click on the link below to validate your participation:</p>")
        .settings.center()
        .print()
    ,
    newText("<p><a href='CONFIRMATION.URL.GOES.HERE' target='_blank'>Validate my participation</a></p>")
        .settings.css("font-size","xxx-large")
        .settings.center()
        .print()
    ,
    newButton("Continue").wait()    // Not printed, so will wait forever
)
.setOption("countsForProgressBar",false)


PennController("consent",
    newHtml("form", "consent.html").settings.css("max-width","50em").print()
    ,
    newButton("Continue")
        .settings.center()
        .print()
        .wait( getHtml("form").test.complete().failure(getHtml("form").warn()) )
)


PennController("instructions",
    newText(`<p>In this study you will learn an alien language called Pinglish. There are two halves to the study.</p>
             <p>In the first half, you'll be shown a set of pictures together with some Pinglish words describing the objects in each scene.
             You will be asked to type the Pinglish words you see in front of you.</p>
             <p>In the second half of the experiment, you'll be tested on your knowledge of Pinglish from the first half,
             which will include applying your knowledge to new objects.</p>
             <p>Please do not write anything down during the experiment. We are interested in how well you learn the language.</p>`)
        .settings.size("50em")
        .print()
    ,   
    newButton("Continue").settings.center().print().wait()
)


PennController("half",
    newText(`<p>You have completed the first half of the experiment.</p>
            <p>Click on the <em>continue</em> button below to proceed to the second half.</p>`)
        .settings.size("50em")
        .print()
    ,   
    newButton("Continue").settings.center().print().wait()
)


PennController("feedback",
    newHtml("form", "feedback.html").settings.css("max-width","50em").settings.log().print()
    ,
    newButton("Continue")
        .settings.center()
        .print()
        .wait( getHtml("form").test.complete().failure(getHtml("form").warn()) )
)
.log("id", PennController.GetURLParameter("id"))